---
description: Vamos a descubrir algo interesante que Uno Platform nos ofrece.
cover: ../.gitbook/assets/SpaceBackground.png
coverY: 0
---

# UI

<figure><img src="../.gitbook/assets/image.png" alt=""><figcaption></figcaption></figure>

Yo he hecho el uso de Figma para desarrollar un bosquejo de lo que sería mi interfaz gráfica de mi juego, podemos trabajar usando Uno Platform siguiendo una seríe de reglas básicas usando un Auto Layout que nos ofrece Figma.

## MainPageView

<figure><img src="../.gitbook/assets/image (1).png" alt=""><figcaption></figcaption></figure>

## HighScoreView

<figure><img src="../.gitbook/assets/image (2).png" alt=""><figcaption></figcaption></figure>

## GameOverView

<figure><img src="../.gitbook/assets/image (3).png" alt=""><figcaption></figcaption></figure>

Y ya Uno Platform tiene un plugin en Figma para poder transformar el diseño de Figma en un diseño funciona de C# para no tener que hacer muchos cambios en tu interfaz. Lo mantuve lo más sencillo posible para poder centrarme en otros aspectos del programa.
