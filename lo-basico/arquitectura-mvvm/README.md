---
description: >-
  Aquí explicaremos a detalle la arquitectura MVVM y como la hemos implementado
  en nuestro video juego
cover: ../../.gitbook/assets/anime-style-galaxy-background.jpg
coverY: 0
---

# Arquitectura MVVM

## Uno Platform en .NET

Uno platform es un framework que extiende las funcionalidades del sistema .NET para multiples plataformes con una sola base de código. Esta utiliza las ventajas que ofrece UWP (Universal Windows Platform) y la extiende a otros sistemas operativos como lo son  iOS, macOs, Android y Linux pero también ofrece ventajas para desarrollar aplicaciones en la web por medio de Web Assembly. Lo que facilita mucho el desarrollo sin la necesidad de trabajar en múltiples lenguajes la misma aplicación.

Uno Platform tiene herramientas diversas para trabajar con .NET como lo que son Visual Studio, Azure, Azure DevOps entre otras herramientas que ofrece .NET para el desarrollo y despligue continuo.

Además lo que hace brillar a uno Platform es el soporte nativo de MVVM, esto es una gran ventaja ya que usarlo tiene una mejor optimización.

## Patrón MVVM

Para comprender un poco de lo que vamos a trabajar hay 3 componentes principales en el patrón MVVM: el modelo, la vista y el modelo vista. Cada uno tiene un propósito diferente.

Uno Platform es un Framework que permite desarrollar aplicaciones mútiplataforma con una base de código, esta es una extensión del ecosistema .NET.

Este utiliza el UWP(Universal Windows Platform) y la extiende a otros sistemas operativos como iOS, Android, MacOS y Linux, también tiene la ventaja de desplegarla en la web a través de WebAssembly. Permitiendo que tengas una aplicación desarrollada con XAML o C# Markup y ejecutarla en diferentes plataformas sin reescribir el código.

Este cuenta con herramientas de .NET que se integran a visual Studio, Azure Devops y otras herrmaientas de .NET lo que abre a una gran variedad de aplicaciones que permiten facilitar el desarrrollo y despliegue continúo.

Además que cuenta con MVVM de manera nativa lo que ofrece soporte y algunas facilidades de usar el patrón MVVM.

<figure><img src="../../.gitbook/assets/image (10).png" alt=""><figcaption></figcaption></figure>

## Interacción de los componentes

Cada componente en el patrón MVVM tiene una responsabilidad específica, y es importante entender cómo interactúan entre sí. En términos generales, la Vista "conoce" al Modelo de Vista y el Modelo de Vista "conoce" al Modelo, pero el Modelo no conoce al Modelo de Vista y el Modelo de Vista no conoce a la Vista. En términos aún más simples, el Modelo de Vista aísla la Vista del Modelo, permitiendo que el Modelo evolucione independientemente de la Vista.

## Ventajas del patrón MVVM

Uno Platform nos brinda un soporte nativo de MVVM, lo que nos ahorra muchísimo trabajo a la hora de trabajar nuestras interfaces, lo que nos brinda las siguientes ventajas:

1. Separación de responsabilidades: permitiendo el mantenimiento y escalabilidad al dividir la lógica del juego.
2. Testeable: al tener la individualidad y la separación de responsabilidades hace más fácil implementar pruebas unitarias.
3. Reutilización de código: facilita la reutilización de código en diferentes vistas.
4. UI: podemos generar cambios en la interfaz gráfica sin afectar la lógica subyacente del juego.

Su clave residen en comprender como se debe factorizar el código de la aplicación en clases concretas y como interactúan las clases.

## Modelo (Model)

Gestiona la lógica y los datos del juego. Define las clases de las entidades del juego como el jugador, enemigos, proyectiles, entidades, obstáculos.

```csharp
namespace SpaceEvolution.Models;
public class PlayerShip
{
    public double PositionShipX { get; set; }

    public PlayerShip()
    {
        PositionShipX = 0;
    }

    public void Move(double movX,  double movY)
    {
        PositionShipX += movX;
    }
}

```



## Modelos base y herencia

Yo implemente como hay lógica repetida en cuanto a los posionamiento descidi crear una clase ShipModelBase que guarda las posiciones.

namespace SpaceEvolution.Models; public class ShipBaseModel : ObservableObject { private double \_baseShipPositionX; private double \_baseShipPositionY;

```csharp
public double BaseShipPositionX
{
    get => _baseShipPositionX;
    set
    {
        if(_baseShipPositionX != value)
        {
            _baseShipPositionX = value;
            OnPropertyChanged();
        }
    }
}

public double BaseShipPositionY
{
    get => _baseShipPositionY;
    set
    {
        if (_baseShipPositionY != value)
        {
            _baseShipPositionY = value;
            OnPropertyChanged();
        }
    }
}

protected ShipBaseModel(double x, double y)
{
    BaseShipPositionX = x;
    BaseShipPositionY = y;
}

public void MovementShip(double movX, double movY)
{
    BaseShipPositionX += movX;
    BaseShipPositionY += movY;
}
}
```

Y ya podemos crear un modelo que hereda de nuestra clase Ship y esta puede tener además sus atributos propios, un ejemplo completo sería mi AlienShip:

```csharp
using System.Drawing;
using SpaceEvolution.Models.Enums;
using SpaceEvolution.Models.Interface;

namespace SpaceEvolution.Models;
public class AlienShipModel : ShipBaseModel, ICoalisionBox
{
    public string ImageAlienSource { get; set; }

    public AlienTierEnum AlienTier { get; set; }

    public AlienShipModel(double x, double y, string imageAlienSource, AlienTierEnum alienTierEnum) : 
    base(x, y)
    {
        ImageAlienSource = imageAlienSource;
        AlienTier = alienTierEnum;
    }

    public Rectangle GetBoundingObjectBox()
    {
        return new Rectangle((int)BaseShipPositionX, (int)BaseShipPositionY, 50, 50);
    }
}

```

Podemos observar que tiene sus atributos propios además y hereda solamente las posiciones de x,y de nuestra clase ShipBaseModel.

## Vista (View)

Se encarga de la representación gráfica de nuestro juego. Podemos dibujar elementos y posterior manejar la lógica detrás como lo puede ser el jugador. Podemos hacer referencia a todos los atributos que esten en la vista y puedan ser asociados al ViewModel.

```xml
<Image x:Name="PlayerShipGame" Source="ms-appx:///Assets/Images/PlayerBase.png" Width="100" Height="100" IsTabStop="True"/>
```

## VistaModelo (ViewModel)

Actúa como el intermediario en nuestro código entre el **Modelo** y la **Vista.** Gestiona la lógica del juego y actualiza la Vista en respuesta a los cambios en el Modelo. En Uno Platform, es recomendable utilizar un ViewModel para manejar la lógica de la vista para mantener la separación de las responsabilidades.

En Uno Platform, es recomendable utilizar un ViewModel para manejar la lógica de la vista para mantener la separación de las responsabilidades.

## Como el Data Binding facilita la actualización de la UI.

El data binding en MVVM permite que la UI se actualice automáticamente en respuesta a los cambios de los datos, esto viene siendo debido a que Uno Platform trae cosas del UWP pero Uno Platform incorpora nuevas herramientas y bibliotecas que facilitan su uso pero vienen siendo completamente lo mismo, pero hablando un poco más a profundidad funciona bien.

Las propiedades que se encuentran en el ViewModel están vinculadas con la UI. Eso sucede que si por ejemplo tenemos una lógica debajo de la vista original pero sino es así tenemos que ligar el ViewModel con la UI a la que queremos gestionar su lógica. Esto se logra a través de la implementación de la interfaz `INotifyPropertyChanged` en el VieeModel.

Esto hace que tenemos una actualización automatica, en este caso por ejemplo si la lógica de mi nave se controla en el GameViewModel entonces el GameView se actualizará gracias al Databinding.

```csharp
public class GameViewModel : ObservableObject
{
    private PlayerShip _playerShip;

    public double PlayerX
    {
        get => _playerShip.PositionShipX;
        set
        {
            if (_playerShip.PositionShipX != value)
            {
                _playerShip.PositionShipX = value;
                OnPropertyChanged();
            }
        }
    }
}

```

Ahora que tenemos en el GameView podemos controlar el movimiento de Izquierda a derecha sin complicaciones y podemos agregar incluso más lógica para gestionar arriba y abajo.

```xml
<Image x:Name="PlayerShipGame" Source="ms-appx:///Assets/Images/PlayerBase.png" Width="100" Height="100" IsTabStop="True">
    <Image.RenderTransform>
        <TranslateTransform X="{Binding PlayerX, Mode=OneWay}" />
    </Image.RenderTransform>
</Image>

```

Y así podemos ir trabajando más conforme vayan apareciendo más métodos y cosas en la UI que necesiten de cierta lógica que manejar.

