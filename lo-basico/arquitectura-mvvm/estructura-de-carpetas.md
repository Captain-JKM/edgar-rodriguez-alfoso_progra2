---
description: Aquí implementamos MVVM en uno Platform
cover: ../../.gitbook/assets/SpaceBackground.png
coverY: 0
---

# Estructura de Carpetas

## Manager

Los manager nos permiten abstraer lógica de nuestro ViewModel para poder hacerlo mantenible ya que podemos tener muchas cosas que pueden sobrecargar  nuestro código haciendolo que sea mantenible nuestro código, estos manejan lógica compleja.

## Services

Los services pueden ocuparse para tener una responsabilidad única para cosas específicas en mi caso solamente la he implementado para hacer el uso del sonido, la base de datos y el tiempo en el juego.

## Helpers

Los helpers son reutilizables no contiene lógica compleja solamente pueden servir para guardar información de variables

## Assets / Images

Imagenes de nuestro juego.

## MVVM

* Model: Lógica y datos del juego
* View: Renderizado y manejo de la interfaz gráfica.
* ViewModel: Intermediario que maneja la lógica de la interfaz y notifica cambios.

## Theme

Archivos de estilo y configuración visual donde podemos guardar imagenes, fonts, estilos que nos pueden servir para ser reutilizados en cualquier momento.



Buscamos aprovechar al máximo las capacidades nativas de MVVM en Uno Platform aunque no es un framework dedicado para video juego tiene cosas puntuales con la cual podemos trabajar.

