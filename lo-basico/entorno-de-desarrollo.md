---
description: >-
  Aquí tratamos aspectos importantes de como trabajamos nuestro repositorio y si
  eres un colaborador sabras como tengo configurado mi estorno para trabajar
  este proyecto.
---

# Entorno de desarrollo

## Gitlab

El proyecto esta en un repositorio de Gitlab donde se van haciendo una serie de commits donde se irá subiendo el avance en el que se irá trabajando estre proyecto.



## IDE

Usaremos la herramienta de  [Visual Studio 2022](https://visualstudio.microsoft.com/es/vs/), que es uno donde puedes programar en C#, F# y VB, puedes hacer uso de frameworks como lo son Entity Framework Core y .NET framework.

Este es un IDE productivo y muy escalable que permite trabajar proyectos de cualquier tamaño y complejidad.

Visual Studio ofrece grandes características que nos sirven para trabajar nuestro proyecto ya que cuenta con una IA muy intuitiva como el IntelliSense para finalizaciones de código, pero para nuestro entorno de desarrollo para nuestro videojuego es bueno, podríamos usar [Unity](https://unity.com/es) pero nos quedamos con este.



## Para desarrolladores

Antes de comenzar nuestro entorno en Windows necesita ser preparado con anterioridad debemos de activar una opción de desarrollador para poder comenzar con esto, para evitar entrar a la configuración de nuestro Windows, solamente ejecuta un powershell en administrador y pega este comando.

```powershell
reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\AppModelUnlock" /t REG_DWORD /f /v "AllowDevelopmentWithoutDevLicense" /d "1"

```

## Uno Platform

Uno Platform es un framework de código abierto basado en .NET que permite a los desarrolladores construir aplicaciones multiplataforma de manera rápida y sencilla. Proporciona una única base de código que puede ejecutarse en múltiples sistemas operativos, incluyendo Windows, macOS, Linux, Android, iOS y navegadores web a través de WebAssembly.

&#x20;Al utilizar la misma base de código para todas estas plataformas, los desarrolladores pueden reducir significativamente el tiempo y los recursos necesarios para crear y mantener aplicaciones. Uno Platform también es compatible con XAML y permite a los desarrolladores reutilizar sus habilidades y componentes de UWP (Universal Windows Platform) en otros entornos, facilitando así la creación de aplicaciones consistentes y de alta calidad en múltiples dispositivos.



Si tienes dudas de como conocer o ininicar un proyecto en Uno Platform puedes consultar este video que dejo aquí

{% embed url="https://www.youtube.com/watch?v=XnAQ6Nrb0Bg" %}

## Instalación de Uno Platform en Windows

## Creación de aplicación de escritorio

A continuación vamos a&#x20;



Una vez instalado de seleccionar el Framework en este caso .NET 8.0

<figure><img src="../.gitbook/assets/image (17).png" alt=""><figcaption></figcaption></figure>

Luego vamos a seleccionar en plataforma solamente dejando Windows,  puedes dejar el resto dependiendo de tus necesidades en este caso yo deje activa todas.



<figure><img src="../.gitbook/assets/image (23).png" alt=""><figcaption></figcaption></figure>



Presentación marcaremos el patrón MVVM.

<figure><img src="../.gitbook/assets/image (19).png" alt=""><figcaption></figcaption></figure>

Los test puedes trabajar con estas configuraciones

<figure><img src="../.gitbook/assets/image (20).png" alt=""><figcaption></figcaption></figure>

El Markup debemos dejarlo en **XAML**&#x20;

Tienes que ver una interfaz como esta y ya podrás empezar a trabajar, asi que ejecutaremos.

<figure><img src="../.gitbook/assets/image (21).png" alt=""><figcaption></figcaption></figure>

Una vez listo tendrás que ver esto

<figure><img src="../.gitbook/assets/image (24).png" alt=""><figcaption></figcaption></figure>

