---
description: >-
  Aquí analizaremos las condiciones de cierre de nuestro juego de Space
  Invaders.
cover: ../.gitbook/assets/wallpaperflare.com_wallpaper.jpg
coverY: 0
---

# GameOver

Bueno hablemos un poco de como nos movemos a la vista de cierre utilizamos un evento de que cuando se dispare podemos crear un evento publico:

```csharp
public event EventHandler GameOverEvent;
```

Podemos guardar el disparador del evento dentro de un método este método podemos usarlo en las diferentes condiciones que podemos declarar para que se acabe el juego.

```csharp
private void GameOver()
{
   StopDistpatchers();
    GameOverEvent?.Invoke(this, EventArgs.Empty);
}
```

Y en nuestro behind del GameView tenemos que agregar un this.load en su constructor y referenciar este método yo lo comprendo para que este escuchando cuando llega este evento que invoca nuestro método.

```csharp
 public GameView()
    {
        this.InitializeComponent();
        this.Loaded += GameView_Load;

        ViewModel.GameOverEvent += OnGameOver;
    }
 private void OnGameOver(object sender, EventArgs e)
 {
     var parameters = new GameOverView.GameOverParameters
     {
         FinalScore = ViewModel.ScoreManager.Score,
         Level = ViewModel.LevelManager.Level.ToString(),
     };
     Frame.Navigate(typeof(GameOverView), parameters);
 }
```

Ahora este GameOver puede ser invocado en las condiciones específicas de nuestro código que disten que el juego debe de finalizar



## Relacionados con lel impacto del jugador

Cada que es impactado nuestro jugador puede perder vidas, si pierde todas entonces se dispara un booleano que dice que el juego se acabo. Sino el jugador es muy bueno y llega al nivel 10 entonces el juego se termina pero sino entonces mala suerte.

```csharp
 ProjectileManager.MoveEnemyProjectiles(PlayerShip, LivesManager, GameOver);

// Dentro de la clae MoveProjectileManager
if (projectile.GetBoundingObjectBox().IntersectsWith(playerShip.GetBoundingBox()))
{
    livesManager.DecreaseLife();
    if (livesManager.IsGameOver)
    {
        gameOverCallback();
    }
    _enemyProjectiles.Remove(projectile);
}

//dentro de livers manager
public bool IsGameOver => Lives <= 0;
```

En el GamerTimer\_Tick en donde se est ejecutando el juego y los hilos podemos comprobar si los aliens han tocado el límite que he establecido en el AliensManager si alcanzan ese rango entonces el juego llama el método de GameOver y también termina, por ende podríamos decir que tenemos 3 condiciones con las cuales el jugador puede terminar el juego.

```csharp
        if (AliensManager.CheckAlienReachedBoundary(PlayerShipYBoundary))
        {
            GameOver();
        }
        if (LevelManager.Level == 10)
        {
            GameOver();
        }
```
