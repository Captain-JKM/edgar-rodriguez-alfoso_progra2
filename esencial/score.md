# Score

Los aliens tienen las siguientes puntuaciones

Special Alien: 100

Attack Ship Alien: 40

Offensive Ship Alien: 30

Protection Ship Alien: 20



## ¿Cómo logre el Score?

Para lograr el score utilice 2 cosas, primero un enum y segundo un método static para lograr el cometido, el Alien Tier Enum se vio en el momento en el que creamos las naves enmigas le asignamos una tier en la cual podemos spawnear.

```csharp

public enum AlienTierEnum
{
    One = 1,
    Two = 2,
    Three = 3,
    Four = 4
}


```

```csharp
using SpaceEvolution.Models.Enums;

namespace SpaceEvolution.Helpers;
public class GameScoreHelper
{
    public static int GetScoreForAlien(AlienTierEnum alienSpawnTier)
    {
        return alienSpawnTier switch
        {
            AlienTierEnum.One => 30,
            AlienTierEnum.Two => 20,
            AlienTierEnum.Four => 100,
            AlienTierEnum.Three => 10
            ,
            _ => 0
        };
    }
}
```

Aqui podemos profundizar más acerca de los projectiles en las cuales pues ya sabemos que están creados desde MainViewModel esto genera una instancia nueva de las balas  lo cual ya sabemos que las balas tienen la posición que se le da al jugador u alien y podemos trabajar solamente en llamar el método correspondiente y trabajar con el. En este caso el movimiento de las balas si se intersecta con un objeto o un borde este genera una acción, en este caso si choca con un objeto este genera puntos.



```csharp
 projectile.MoveProjectile();
 var hitAlien = alienShips.FirstOrDefault(alien => alien.GetBoundingObjectBox().IntersectsWith(projectile.GetBoundingObjectBox())); 
 if (hitAlien != null)
 {
     int points = GameScoreHelper.GetScoreForAlien(hitAlien.AlienTier);
     scoreManager.AddPoints(points);
     alienShips.Remove(hitAlien);
     _playerProjectiles.Remove(projectile);
 }
 if (specialAlien.GetBoundingObjectBox().IntersectsWith(projectile.GetBoundingObjectBox()))
 {
     int points = GameScoreHelper.GetScoreForAlien(specialAlien.AlienTier);
     scoreManager.AddPoints(points);
     specialAlien.BaseShipPositionX = -200;
 }
```
