---
cover: ../.gitbook/assets/beautiful-milky-way-night-sky.jpg
coverY: 0
---

# Manejo de archivos

Para guardar la información de nuestro jugadores en un score no lo maneje de manera local hice uso de Mysql.Data que nos permite manejar operaciones sql que no sea a nivel de arquitectura.

```csharp
    public void SaveScoreToDatabase(string playerName, int score)
    {
        string connectionString = "connectin";
        using (MySqlConnection conn = new MySqlConnection(connectionString))
        {
            try
            {
                conn.Open();
                string query = "INSERT INTO HighScores (PlayerName, Score, Date) VALUES (@PlayerName, @Score, @Date)";
                using (MySqlCommand cmd = new MySqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@PlayerName", playerName);
                    cmd.Parameters.AddWithValue("@Score", score);
                    cmd.Parameters.AddWithValue("@Date", DateTime.Now);

                    cmd.ExecuteNonQuery();
                }
            }
            catch (MySqlException ex)
            {
                ContentDialog errorDialog = new ContentDialog()
                {
                    Title = "Error",
                    Content = $"Fallo guardar el Score: {ex.Message}",
                    CloseButtonText = "OK"
                };
                errorDialog.ShowAsync();
            }
        }
    }
```

## Para traer el score

```csharp
    public static List<HighScoreEntry> GetHighScores()
    {
        var highScores = new List<HighScoreEntry>();
        string connectionString = "connection";
        using (MySqlConnection conn = new MySqlConnection(connectionString))
        {
            try
            {
                conn.Open();
                string query = "SELECT PlayerName, Score, Date FROM HighScores ORDER BY Score DESC LIMIT 10";
                using (MySqlCommand cmd = new MySqlCommand(query, conn))
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            highScores.Add(new HighScoreEntry
                            {
                                PlayerName = reader["PlayerName"].ToString(),
                                Score = Convert.ToInt32(reader["Score"]),
                                Date = Convert.ToDateTime(reader["Date"])
                            });
                        }
                    }
                }
            }
            catch (MySqlException ex)
            {
                ContentDialog errorDialog = new ContentDialog()
                {
                    Title = "Error",
                    Content = $"Error en mostrar el Score: {ex.Message}",
                    CloseButtonText = "OK"
                };
                errorDialog.ShowAsync();
            }
        }

        return highScores;
    }

    public class HighScoreEntry
    {
        public string PlayerName { get; set; }
        public int Score { get; set; }
        public DateTime Date { get; set; }
    }
}
```

## Guardar el Score

Por medio de un ContentDialog guardamos la información de nuestro jugador actual, lo que nos permite pues guardar su score, el date  y los puntos alcanzadosl

```csharp
private async void GameOverView_Loaded(object sender, RoutedEventArgs e)
{
    var dialog = new PlayerNameDialog();
    dialog.XamlRoot = this.XamlRoot;
    var result = await dialog.ShowAsync();

    if (result == ContentDialogResult.Primary)
    {
        string playerName = dialog.PlayerName;

        PlayerName.Text = playerName;

        FinalScore.Text = $"{_finalScore.ToString()} Puntos";

        LevelReach.Text = $"Has alcanzado {_level} niveles";

        if (!string.IsNullOrEmpty(playerName))
        {
            ScoreDbService.SaveScoreToDatabase(playerName, _finalScore);
            
            ContentDialog successDialog = new ContentDialog()
            {
                Title = "Success",
                Content = "Score ha sido guardado!",
                CloseButtonText = "OK",
                XamlRoot = this.XamlRoot
            };
            await successDialog.ShowAsync();
        }
    }
}
```

## Guardar el archivo de puntuaciones

En la ventana de HighScore mostramos las puntuaciones de la base de datos con un método estático con el que vamos a mostrar las puntuaciones que obtuvimos.

```csharp
    private void ShowHighScores()
    {
        List<HighScoreEntry> highScores = GetHighScores();

        HighScoreListView.ItemsSource = highScores;
    }
```

## Descargar el archivo

Hacemos un uso de un stringbuilder y por medio de una ruta temporal podemos generar un archivo con el que podemos guardar el score sin comprometer pues a nuestros usuarios guardar algo que no quieren hacer en automático en sus equipos.

```csharp
 private async void DownloadInfo(object sender, TappedRoutedEventArgs e)
 {

     List<HighScoreEntry> highScores = GetHighScores();

     StringBuilder csvContent = new StringBuilder();
     csvContent.AppendLine("PlayerName,Score,Date");

     foreach (var score in highScores)
     {
         csvContent.AppendLine($"{score.PlayerName},{score.Score},{score.Date}");
     }

     var localFolder = ApplicationData.Current.TemporaryFolder;
     var file = await localFolder.CreateFileAsync("HighScores.csv", CreationCollisionOption.ReplaceExisting);
     await FileIO.WriteTextAsync(file, csvContent.ToString());

     await Launcher.LaunchFileAsync(file);
 }
```
