---
description: >-
  Este es un proyecto de programación 3 desarrollado por Edgar Vicente Rodríguez
  Alfonso, estaremos desarrollando el juego Space Invaders con mi toque personal
  para que sea un experiencia recreativa.
cover: .gitbook/assets/fondo-galaxia-estilo-anime.png
coverY: 0
---

# Space Evolution - Overview

## Prueba mi proyecto

[Link Media Fire](https://www.mediafire.com/file/xewouojtsasia3e/Space+Invaders.zip/file)

Necesitas tener la rutine de .NET para poder usarlo y poder jugarlo es un portable de momento.

## Introducción

Nuestro objetivo es hacer una recreación del juego de "Spaces Invaders", creada para ser una aplicación de escritorio desarrollando usando Uno Platform para ecosistemas .NET en lenguaje C#.

Donde se demostrará el uso de componentes visuales e interactuar mediante eventos, manejar procesos (movimientos), responsive, archivos, mecanismos y demás.



Además como el uso de buenas prácticas, patrones de diseño y una arquitectura MVVM se hicieron uso para el desarrollo de este video juego.



Eperamos que lo disfrutes.

## Elementos clave

#### Aspectos del game

* Pantalla de inicio
* Naves

#### Aspectos del jugador

* Movimiento de izquierda a derecha por medio de las flechas del teclado.
* Botón espacio para atacar.

#### Alienigenas

* Desplazamiento de izquierda a derecha.
* Cada que tocan los bordes se mueven abajo y la velocidad aumenta.
* La única nave que ataca vale 40Pts.
  * Los alienigenas tienen 10,20,30, 100 puntos.
* Las naves rojas se mueven aleatoriamente con un rango de aparición de 1-2 minutos.

## &#x20;Condiciones elementos y extra

El juego termina cuando las vidas del jugador se acaben. Otra condición es cuando las naves alacancen al jugador o este termine con todas las naves y gane. También si el jugador llega al nivel 10 este juego también se acabe.

Un elemento importante es que el jugador tiene 4 bloques de protección. Donde los alienigenas no pueden colisionar con los mismos, sino hasta alcanzar al jugador.

Aumento de dificultad gradual por medio de que cada vez que una nueva oleada de aliens llegue su velocidad aumente.



## Diagrama de clases

```plant-uml
@startuml
class GameObject {
+ OnPropertyChanged()
  }

class AnimationTextHelper {
+ StartTextAnimation(TextBlock targetTextBlock)
  }

class GameTimeService {
+ CurrentTimeGame
- UpdateGameTime
}

class ShipBaseModel {
+ double BaseShipPositionX
+ double BaseShipPositionY
- ShipBaseModel (double x, double y)
+ MovementShip(double movX, double movY)
  }

class EnemyProjectileModel {
}

class AlienShipModel {
+ string ImageAlienSource
+ AlienTierEnum AlienTier
+ GetBoundingObjectBox(): Rectangle
  }

class PlayerShipModel {
+ IWeapon Weapon
+ GetBoundingBox(): Rectangle
+ Shoot()
  }

interface IWeapon {
+ void shoot
}

interface ICoalisionBox {
+ rectangle
}

class AliensManager {
+ ObservableCollection<AlienShipModel> alienShips
+ SpawnAlienGrid(int level)
+ MoveAliens()
  }

class ProjectileManager {
+ MovePlayerProjectiles(ObservableCollection<AlienShipModel> alienShips, AlienShipModel specialAlien, ScoreManager scoreManager)
+ MoveEnemyProjectiles(PlayerShipModel playerShip, LivesManager livesManager, Action gameOverCallback)
  }

enum AlienTierEnum{
 AlienTierEnum.One => 10,
 AlienTierEnum.Two => 20,
 AlienTierEnum.Four => 100,
 AlienTierEnum.Three => 30
}

class ScoreManager {
+ int Score
+ AddPoints(int points)
  }

class ScoreDbService {
+SaveScoreToDatabase(string playerName, int score)
+ List<HighScoreEntry> GetHighScores()
}

GameObject <|-- ShipBaseModel
GameObject <|-- ScoreManager
GameObject <|-- LevelManager
GameObject <|-- LivesManager
GameObject <|-- GameTimeService
ShipBaseModel <|-- AlienShipModel
ShipBaseModel <|-- PlayerShipModel
PlayerShipModel --> IWeapon
AlienShipModel --> ICoalisionBox
AlienShipModel --|> AlienTierEnum
PlayerShipModel --> ICoalisionBox
ProjectileModel --> ICoalisionBox
ProjectileModel <|-- EnemyProjectilModel
GameObject <|-- GameViewModel
GameViewModel --> PlayerShipModel
GameViewModel --> LivesManager
GameViewModel --> LevelManager
GameViewModel --> ScoreManager
GameViewModel --> ProjectileManager
GameViewModel --> AliensManager


MainPageViewModel --> GameTimeService
MainPage --> MainPageViewModel
MainPageViewModel --> AnimationTextHelper
HighScoreView --> ScoreDbService
GameViewModel --> ProjectileModel
GameView --> GameViewModel
GameViewModel --> ProjectileModel
GameViewModel --> EnemyProjectileModel
GameView --> GameOverView
GameOverView --> GameViewModel
GameOverView --> ScoreDbService
@enduml
```



