package edgarvicentera.progra2.labs.labthree;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

public class LogTest {

    private CalculadoraAvanzada calculadora;

    @BeforeEach
    public void setUp() {
        Estudiante student = new Estudiante("Diana", "Romero", "20230001");
        calculadora = new CalculadoraAvanzada(student);
    }

    @Test
    public void testResolverEcuacionCuadratica() {
        double[] expected = { -1.0, -2.0 };
        assertArrayEquals(expected, calculadora.resolverEcuacionCuadratica(1, 3, 2), 0.01);
    }

    @Test
    public void testGenerarSecuenciaFibonacci() {
        List<Integer> expected = Arrays.asList(0, 1, 1, 2, 3, 5, 8);
        assertEquals(expected, calculadora.generarSecuenciaFibonacci(7));
    }
   
}
