package edgarvicentera.progra2.labs.labthree;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class GeometricAbstractTest {

    @Test
    public void testCalcularArea() {
        // Arrange
        GeometricAbstract geometricShape = new GeometricAbstract() {
            @Override
            public double calcularArea() {
                return 10.0; // Área de prueba arbitraria
            }

            @Override
            public double calcularPerimetro() {
                return 0.0; // No es necesario para este caso de prueba
            }
        };

        // Act
        double area = geometricShape.calcularArea();

        // Assert
        assertEquals(10.0, area, 0.01); // Área esperada
    }

    @Test
    public void testCalcularPerimetro() {
        // Arrange
        GeometricAbstract geometricShape = new GeometricAbstract() {
            @Override
            public double calcularArea() {
                return 0.0; // No es necesario para este caso de prueba
            }

            @Override
            public double calcularPerimetro() {
                return 20.0; // Perímetro de prueba arbitrario
            }
        };

        // Act
        double perimetro = geometricShape.calcularPerimetro();

        // Assert
        assertEquals(20.0, perimetro, 0.01); // Perímetro esperado
    }
}
