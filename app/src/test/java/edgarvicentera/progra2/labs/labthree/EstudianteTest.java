package edgarvicentera.progra2.labs.labthree;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class EstudianteTest {

    @Test
    public void testImprimir() {
        // Arrange
        Estudiante estudiante = new Estudiante("Juan", "Perez", "20230001");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;

        // Act
        System.setOut(new PrintStream(outputStream));
        estudiante.imprimir();
        System.setOut(originalOut);

        // Assert
        String expectedOutput = "Nombre: Juan, Apellido: Perez, Matricula: 20230001";
        assertEquals(expectedOutput, outputStream.toString().trim());
    }
}
