package edgarvicentera.progra2.labs.labthree;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.List;
import java.util.ArrayList; 

public class GenericSequenceTest {

    @Test
    public void testGenerarSecuencia() {
        
        GenericSecuence<Integer> sequenceGenerator = new GenericSecuence<Integer>() {
            @Override
            public List<Integer> generarSecuencia(int n) {
                
                List<Integer> sequence = new ArrayList<>();
                for (int i = 1; i <= n; i++) {
                    sequence.add(i);
                }
                return sequence;
            }
        };

        
        List<Integer> sequence = sequenceGenerator.generarSecuencia(5);

        // Assert
        assertNotNull(sequence);
        assertEquals(5, sequence.size());
        assertEquals(List.of(1, 2, 3, 4, 5), sequence);
    }
}
