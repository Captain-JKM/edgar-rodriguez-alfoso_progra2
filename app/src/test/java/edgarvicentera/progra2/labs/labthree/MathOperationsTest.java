package edgarvicentera.progra2.labs.labthree;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MathOperationsTest {

    @Test
    public void testCalculate() {
        // Crear una instancia 
        MathOperations mathOperations = new BasicOperators.Suma();

        // Llamar al método calculate 
        double result = mathOperations.calculate(2.0, 3.0);
        assertEquals(5.0, result); 
    }
}
