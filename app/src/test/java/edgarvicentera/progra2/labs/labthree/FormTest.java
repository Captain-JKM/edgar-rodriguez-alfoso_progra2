package edgarvicentera.progra2.labs.labthree;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FormTest {

    @Test
    public void testEnumValues() {
        
        Form[] expectedValues = {Form.CIRCULO, Form.CUADRADO, Form.TRIANGULO, Form.RECTANGULO};

        Form[] actualValues = Form.values();

        assertArrayEquals(expectedValues, actualValues);
    }
}
