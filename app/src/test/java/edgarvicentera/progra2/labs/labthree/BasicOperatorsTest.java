package edgarvicentera.progra2.labs.labthree;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class BasicOperatorsTest {

    @Test
    void testSuma() {
        // Creamos una instancia de la clase Suma
        BasicOperators.Suma suma = new BasicOperators.Suma();
        
        // Llamamos al método calculate en la instancia creada
        double resultado = suma.calculate(2, 3, 5);
        
        // Verificamos si el resultado es el esperado
        assertEquals(10, resultado, "La suma no es correcta");
    }

    @Test
    void testSubtract() {
        int resultado = BasicOperators.subtract(10, 7);
        assertEquals(3, resultado, "La resta no es correcta");
    }

    @Test
    void testMultiply() {
        int resultado = BasicOperators.multiply(4, 6);
        assertEquals(24, resultado, "La multiplicación no es correcta");
    }

    @Test
    void testDivide() {
        int resultado = BasicOperators.divide(20, 4);
        assertEquals(5, resultado, "La división no es correcta");
    }

    @Test
    void testDividePorCero() {
        assertThrows(ArithmeticException.class, () -> {
            BasicOperators.divide(10, 0);
        }, "No se lanzó la excepción esperada para división por cero");
    }
}
