package edgarvicentera.progra2.labs.labthree;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.List;

public class CalculadoraAvanzadaTest {

    @Test
    public void testSumar() {
        CalculadoraAvanzada calculadora = new CalculadoraAvanzada(null);
        assertEquals(15, calculadora.sumar(10, 5));
    }

    @Test
    public void testRestar() {
        CalculadoraAvanzada calculadora = new CalculadoraAvanzada(null);
        assertEquals(2, calculadora.restar(5, 3));
    }

    @Test
    public void testMultiplicar() {
        CalculadoraAvanzada calculadora = new CalculadoraAvanzada(null);
        assertEquals(20, calculadora.multiplicar(4, 5));
    }

    @Test
    public void testDividir() {
        CalculadoraAvanzada calculadora = new CalculadoraAvanzada(null);
        assertEquals(4, calculadora.dividir(20, 5));
    }

    @Test
    public void testResolverEcuacionCuadratica() {
        CalculadoraAvanzada calculadora = new CalculadoraAvanzada(null);
        double[] expected = { -1.0, -2.0 };
        assertArrayEquals(expected, calculadora.resolverEcuacionCuadratica(1, 3, 2), 0.01);
    }

    @Test
    public void testGenerarSecuenciaFibonacci() {
        CalculadoraAvanzada calculadora = new CalculadoraAvanzada(null);
        List<Integer> expected = Arrays.asList(0, 1, 1, 2, 3, 5, 8);
        assertEquals(expected, calculadora.generarSecuenciaFibonacci(7));
    }

    @Test
    public void testGenerarSecuenciaNumerosAleatorios() {
        CalculadoraAvanzada calculadora = new CalculadoraAvanzada(null);
        List<Integer> expected = Arrays.asList(4, 9, 2, 7, 1);
        assertEquals(expected.size(), calculadora.generarSecuenciaNumerosAleatorios(5).size());
        // Aquí puedes comparar el contenido de la lista, ya que es aleatorio
    }

}
