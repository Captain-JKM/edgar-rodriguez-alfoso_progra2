package edgarvicentera.progra2.labs.labtwo;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CalculadoraAvanzadaTest {

    @Test
    public void testCalculadoraAvanzada() {
        CalculadoraAvanzada calculadoraAvanzada = new CalculadoraAvanzada();

        // Prueba de operaciones básicas
        assertEquals(5, calculadoraAvanzada.getBasicOperators().add(2, 3));
        assertEquals(2, calculadoraAvanzada.getBasicOperators().subtract(5, 3));
        assertEquals(15, calculadoraAvanzada.getBasicOperators().multiply(3, 5));
        assertEquals(2, calculadoraAvanzada.getBasicOperators().divide(10, 5));

        // Prueba de área de círculo
        assertEquals(Math.PI * 9, calculadoraAvanzada.getCircleAreaCalculator().calculateArea(3));

        // Prueba de funciones trigonométricas
        assertEquals(0.0, TrigonometricCalculator.calculateSine(0));
        assertEquals(1.0, TrigonometricCalculator.calculateCosine(0));


        // Prueba de ecuación cuadrática
        assertArrayEquals(new double[]{2.0, -3.0}, calculadoraAvanzada.getQuadraticEquationSolution().solveQuadraticEquation(1, 1, -6));

        // Prueba de secuencia de Fibonacci
        assertArrayEquals(new int[]{0, 1, 1, 2, 3, 5, 8, 13, 21, 34},
                calculadoraAvanzada.getFibonacciSequence().generateSequence(10).stream()
                        .mapToInt(Integer::intValue)
                        .toArray());

        // Prueba de secuencia de números aleatorios
        int[] randomSequence = calculadoraAvanzada.getRandomNumberSecuence()
                .generateRandomSequence(5, 1, 10);
        assertEquals(5, randomSequence.length);

      }
}
