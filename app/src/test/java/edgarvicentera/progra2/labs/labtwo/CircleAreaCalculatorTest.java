package edgarvicentera.progra2.labs.labtwo;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CircleAreaCalculatorTest {
    

    @Test
    public void testCalcularArea() {
        CircleAreaCalculator circleAreaCalculator = new CircleAreaCalculator();
        assertEquals(Math.PI * 25.0, circleAreaCalculator.calculateArea(5.0), 0.001);
    }

}
