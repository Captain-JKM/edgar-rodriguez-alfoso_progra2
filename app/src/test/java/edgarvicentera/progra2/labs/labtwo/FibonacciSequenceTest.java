package edgarvicentera.progra2.labs.labtwo;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

public class FibonacciSequenceTest {

    @Test
    public void testGenerateSequence() {
        FibonacciSequence fibonacciSequence = new FibonacciSequence();

        // Verificar la secuencia para n = 0
        List<Integer> sequenceForZero = fibonacciSequence.generateSequence(0);
        assertTrue(sequenceForZero.isEmpty(), "La secuencia para n = 0 debe estar vacía");

        // Verificar la secuencia para n = 1
        List<Integer> sequenceForOne = fibonacciSequence.generateSequence(1);
        assertEquals(List.of(0), sequenceForOne, "La secuencia para n = 1 debe ser [0]");

        // Constatar el numero 5 en la secuencia
        List<Integer> sequenceForFive = fibonacciSequence.generateSequence(5);
        assertEquals(List.of(0, 1, 1, 2, 3), sequenceForFive, "La secuencia para n = 5 debe ser [0, 1, 1, 2, 3]");
        
        
        
    }
}