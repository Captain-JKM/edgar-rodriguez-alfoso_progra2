package edgarvicentera.progra2.labs.labtwo;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class BasicOperatorsTest {

    @Test
    public void testAdd() {
        BasicOperators basicOperators = new BasicOperators();
        assertEquals(7, basicOperators.add(4, 3), "La suma de 4 y 3 debería ser 7");
    }

    @Test
    public void testSubtract() {
        BasicOperators basicOperators = new BasicOperators();
        assertEquals(2, basicOperators.subtract(8, 6), "La resta de 8 y 6 debería ser 2");
    }

    @Test
    public void testMultiply() {
        BasicOperators basicOperators = new BasicOperators();
        assertEquals(18, basicOperators.multiply(3, 6), "La multiplicación de 3 y 6 debería ser 18");
    }

    @Test
    public void testDivide() {
        BasicOperators basicOperators = new BasicOperators();
        assertEquals(3, basicOperators.divide(15, 5), "La división de 15 por 5 debería ser 3");
    }

    @Test
    public void testDivideByZero() {
        BasicOperators basicOperators = new BasicOperators();
        assertThrows(
            ArithmeticException.class,
            () -> basicOperators.divide(10, 0),
            "La división por cero debería lanzar una excepción ArithmeticException"
        );
    }
}
