package edgarvicentera.progra2.labs.labtwo;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TrigonometricCalculatorTest {

    @Test
    public void testCalculateSine() {
        assertEquals(0.7071, TrigonometricCalculator.calculateSine(45.0), 0.0001);
    }

    @Test
    public void testCalculateCosine() {
        assertEquals(0.7071, TrigonometricCalculator.calculateCosine(45.0), 0.0001);
    }

    @Test
    public void testCalculateTangent() {
        assertEquals(1.0, TrigonometricCalculator.calculateTangent(45.0), 0.0001);
    }


}
