package edgarvicentera.progra2.labs.labtwo;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

public class QuadraticEquationSolverTest {

    @Test
    public void testSolveQuadraticEquation() {
        QuadraticEquationSolver solver = new QuadraticEquationSolver();

        // Discriminates + (dos soluciones reales distintas)
        assertTrue(Arrays.equals(new double[]{-1.0, -2.0}, solver.solveQuadraticEquation(1, 3, 2)));

        // Discriminante indetermindor (una solución real, raíz doble)
        assertTrue(Arrays.equals(new double[]{-1.0}, solver.solveQuadraticEquation(1, 2, 1)));

        // Discriminante -  (sin soluciones reales)
        assertTrue(Arrays.equals(new double[]{}, solver.solveQuadraticEquation(1, 1, 1)));
    }
}
