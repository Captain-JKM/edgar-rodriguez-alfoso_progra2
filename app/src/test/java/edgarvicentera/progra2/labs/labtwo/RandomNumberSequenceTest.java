package edgarvicentera.progra2.labs.labtwo;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class RandomNumberSequenceTest {

    @Test
    public void testGetNextRandomInt() {
        RandomNumberSequence randomNumberSequence = new RandomNumberSequence();

        int firstRandom = randomNumberSequence.getNextRandomInt();
        int secondRandom = randomNumberSequence.getNextRandomInt();

        assertNotEquals(firstRandom, secondRandom, "Los números aleatorios no deben ser iguales");
    }
}
