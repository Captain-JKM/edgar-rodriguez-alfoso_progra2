package edgarvicentera.progra2.labs.labtwo;

public class CircleAreaCalculator {
  // destacamos los argumentos que vamos a pedir en esta clase
  public double calculateArea(double radio) {
        return Math.PI * Math.pow(radio, 2);

  }
}