package edgarvicentera.progra2.labs.labtwo;

import java.util.Random;

public class RandomNumberSequence {

    private final Random random;

    public RandomNumberSequence() {
        this.random = new Random();
    }

    public int getNextRandomInt() {
        return random.nextInt();
    }


    public int[] generateRandomSequence(int length, int min, int max) {
        if (length <= 0) {
            throw new IllegalArgumentException("La longitud debe ser mayor que cero");
        }

        int[] randomSequence = new int[length];
        for (int i = 0; i < length; i++) {
            randomSequence[i] = random.nextInt((max - min) + 1) + min;
        }
        return randomSequence;
    }
}
