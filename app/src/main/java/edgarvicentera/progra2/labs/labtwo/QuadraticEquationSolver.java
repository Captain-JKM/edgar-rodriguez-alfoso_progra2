package edgarvicentera.progra2.labs.labtwo;

public class QuadraticEquationSolver {

    /**
     * Resuelve una ecuación cuadrática de la forma ax^2 + bx + c = 0
     * y devuelve las soluciones reales, por ende debemos trabajar la formula para
     * poder llegar a la forma correcpondiente y poder usarla, es sencilla
     *
     * @param a Coeficiente cuadrático
     * @param b Coeficiente lineal
     * @param c Término independiente
     * @return Arreglo de soluciones reales
     * 
     * 
     * El discriminante puede tomar 3 casos:
     * 
     * - tiende a positivo = dos soluciones
     * - tiende a negativo = a una solición
     * - tiende sin soluciones reales = no hay solución
     */


    public double[] solveQuadraticEquation(double a, double b, double c) {

        // Calculo del discriminante de la forma b^2 - 4ac 

        double discriminant = b * b - 4 * a * c;

        if (discriminant > 0) {

            // Dos soluciones reales distintas
            double root1 = (-b + Math.sqrt(discriminant)) / (2 * a);
            double root2 = (-b - Math.sqrt(discriminant)) / (2 * a);
            return new double[]{root1, root2};
        } else if (discriminant == 0) {

            // Una solución real (raíz doble)
            double root = -b / (2 * a);
            return new double[]{root};
        } else {
            
            // Sin soluciones reales
            return new double[]{};
        }
    }
}
