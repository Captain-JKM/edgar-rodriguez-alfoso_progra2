package edgarvicentera.progra2.labs.labtwo;

public class CalculadoraAvanzada {

    private BasicOperators basicOperators;
    private CircleAreaCalculator circleAreaCalculator;
    private TrigonometricCalculator trigonometricCalculator;
    private QuadraticEquationSolver quadraticEquationSolution;
    private FibonacciSequence fibonacciSequence;
    private RandomNumberSequence randomNumberSecuence;

    public CalculadoraAvanzada() {
        this.basicOperators = new BasicOperators();
        this.circleAreaCalculator = new CircleAreaCalculator();
        this.trigonometricCalculator = new TrigonometricCalculator();
        this.quadraticEquationSolution = new QuadraticEquationSolver();
        this.fibonacciSequence = new FibonacciSequence();
        this.randomNumberSecuence = new RandomNumberSequence();
    }

    // Métodos para acceder a las operaciones de cada clase
    public BasicOperators getBasicOperators() {
        return basicOperators;
    }

    public CircleAreaCalculator getCircleAreaCalculator() {
        return circleAreaCalculator;
    }

    public TrigonometricCalculator getTrigonometricCalculator() {
        return trigonometricCalculator;
    }

    public QuadraticEquationSolver getQuadraticEquationSolution() {
        return quadraticEquationSolution;
    }

    public FibonacciSequence getFibonacciSequence() {
        return fibonacciSequence;
    }

    public RandomNumberSequence getRandomNumberSecuence() {
        return randomNumberSecuence;
    }
}
