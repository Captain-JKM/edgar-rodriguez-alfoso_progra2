package edgarvicentera.progra2.labs.labone;
/**
 * LabOne class.
 */
public class LabOne {

    /**
     * main method.
     *
     * @param args args.
     */
    public static void main(String[] args) {
        var res = process(args);
      System.out.printf(res);
    }
  
    static String process(String[] args) {
      var res = "";
      var o = new String[]{"op", "alg", "input", "ct-p", "ct-r"}; //adaptamos la nueva funcionalidad siguiendo un poco la estrucutura original
      var v = new String[]{null, null, null, null, null};

      if (args.length == 2 && args[0].startsWith("-") && args[1].startsWith("-")) {
          var key1 = args[0].substring(1, args[0].indexOf("="));
          var key2 = args[1].substring(1, args[1].indexOf("="));
          if ("input".equals(key1) && "op".equals(key2)
                  && ("e".equals(args[1].substring(args[1].indexOf("=") + 1))
                  || "d".equals(args[1].substring(args[1].indexOf("=") + 1)))) {
              res = args[0].substring(args[0].indexOf("=") + 1);
          } else if ("input".equals(key2) && "op".equals(key1)
                  && ("e".equals(args[0].substring(args[0].indexOf("=") + 1))
                  || "d".equals(args[0].substring(args[0].indexOf("=") + 1)))) {
              res = args[1].substring(args[1].indexOf("=") + 1);
          }
      } else if (args.length >= 3 && args[0].startsWith("-")
              && args[1].startsWith("-") && args[2].startsWith("-")) {
          for (int i = 0; i < o.length; i++) {
              for (int j = 0; j < args.length; j++) {
                  var key = args[j].substring(1, args[j].indexOf("="));
                  if (o[i].equals(key)) {
                      v[i] = args[j].substring(args[j].indexOf("=") + 1);
                      break;
                  }
              }
          }

          String opv = null;
          for (int i = 0; i < o.length; i++) {
              if ("op".equals(o[i])) {
                  opv = v[i];
                  break;
              }
          }

          String algv = null;
          for (int i = 0; i < o.length; i++) {
              if ("alg".equals(o[i])) {
                  algv = v[i];
                  break;
              }
          }

          String input = null;
          for (int i = 0; i < o.length; i++) {
              if ("input".equals(o[i])) {
                  input = v[i];
                  break;
              }
          }
        //funcionalidad extra 
          String ctP = null;
          for (int i = 0; i < args.length; i++) {
              if (args[i].startsWith("--ct-p=")) {
                  ctP = args[i].substring(args[i].indexOf("=") + 1);
                  break;
              }
          }

          String ctR = null;
          for (int i = 0; i < args.length; i++) {
              if (args[i].startsWith("--ct-r=")) {
                  ctR = args[i].substring(args[i].indexOf("=") + 1);
                  break;
              }
          }
        //fin de la funcionalidad extra

          if ("none".equals(algv) && ("e".equals(opv) || "d".equals(opv))) {
              res = input;
          } else if ("alg1".equals(algv)) {
              String key = "ZYXWVUTSRQPONMLKJIHGFEDCBA";
              if ("e".equals(opv)) {
                  StringBuilder sb = new StringBuilder();
                  for (char ch : input.toCharArray()) {
                      if (Character.isLetter(ch)) {
                          char uch = Character.toUpperCase(ch);
                          int index = uch - 'A';
                          char ench = index >= 0 && index < key.length() ? key.charAt(index) : ch;

                          sb.append(Character.isLowerCase(ch) ? Character.toLowerCase(ench) : ench);
                      } else {
                          sb.append(ch);
                      }
                  }

                  res = sb.toString();

              } else if ("d".equals(opv)) {
                  StringBuilder sb = new StringBuilder();
                  for (char ch : input.toCharArray()) {
                      if (Character.isLetter(ch)) {
                          char uch = Character.toUpperCase(ch);
                          int index = key.indexOf(uch);
                          char dch = index >= 0 && index < key.length() ? (char) ('A' + index) : ch;
                          sb.append(Character.isLowerCase(ch) ? Character.toLowerCase(dch) : dch);
                      } else {
                          sb.append(ch);
                      }
                  }

                  res = sb.toString();
              }
          } else if ("alg2".equals(algv)) {
              if ("e".equals(opv) || "d".equals(opv)) {
                  String hs = "ZYXWVUTSRQPONMLKJIHGFEDCBA";
                  StringBuilder sb = new StringBuilder();
                  for (char ch : input.toCharArray()) {
                      for (char hsch : hs.toCharArray()) {
                          ch = (char) (ch ^ hsch);
                      }
                      sb.append(ch);
                  }
                  res = sb.toString();
              }
          } else if ("ct".equals(algv) && "e".equals(opv) && ctR == null) {
              //detecta el argumento que esta entrando
              System.out.println("depuracion");
              System.out.println("Valor de ctP: " + ctP);
              if (ctP != null) {
                  System.out.println("segundo nivel"); //nos da indica que el resultado
                  StringBuilder sb = new StringBuilder();
                  sb.append(ctP).append(input).append(ctP);  // en este caso nos da la entrada para abc
                  res = sb.toString();
              }
          } else if ("ct".equals(algv) && "d".equals(opv)) {
              if (ctP != null) {
                  res = input.substring(ctP.length(), input.length() - ctP.length());
              }
          } else if ("ct".equals(algv) && "e".equals(opv) && ("true".equals(ctR) || ctR == null)) {
              StringBuilder sb = new StringBuilder(input).reverse();
              res = sb.toString();
              //podemos ir adaptando para los nuevos argumentos
          } else if ("ct".equals(algv) && "d".equals(opv) && ("true".equals(ctR) || ctR == null)) {
              StringBuilder sb = new StringBuilder(input).reverse();
              res = sb.toString();
          }
      }

      return res;
  }
}