package edgarvicentera.progra2.labs.labthree;

public class BasicOperators {
    public static class Suma implements MathOperations {
        @Override
        public double calculate(double... operandos) {
            double resultado = 0;
            for (double operando : operandos) {
                resultado += operando;
            }
            return resultado;
        }
    
    
    }

    public static class Resta implements MathOperations {
        @Override
        public double calculate(double... operandos) {
            double resultado = 0;
            for (double operando : operandos) {
                resultado -= operando;
            }
            return resultado;
        }
    }

    public static int subtract(int a, int b) {
        return a - b;
    }

    public static int multiply(int a, int b) {
        return a * b;
    }

    public static int divide(int a, int b) {
        if (b == 0) {
            throw new ArithmeticException("No podemos dividir por cero"); 
        }
        return a / b;
    }
}
