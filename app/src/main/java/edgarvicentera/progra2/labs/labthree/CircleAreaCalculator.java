package edgarvicentera.progra2.labs.labthree;

public class CircleAreaCalculator extends GeometricAbstract {
    
    private double radio;

    public CircleAreaCalculator(double radio) {
        this.radio = radio;
    }

    public CircleAreaCalculator(int radio, Form forma) {
        this.radio = radio;
        if (forma != Form.CIRCULO) {
            throw new IllegalArgumentException("La forma debe ser un círculo");
        }
    }

    @Override
    public double calcularArea() {
        return Math.PI * Math.pow(radio, 2);
    }

    @Override
    public double calcularPerimetro() {
        return 2 * Math.PI * radio;
    }
}
