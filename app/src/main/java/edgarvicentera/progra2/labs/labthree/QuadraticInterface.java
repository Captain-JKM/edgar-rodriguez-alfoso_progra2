package edgarvicentera.progra2.labs.labthree;

public abstract class QuadraticInterface {
  public abstract double operar(double a, double b);
}