package edgarvicentera.progra2.labs.labthree;


public interface MathOperations {
    double calculate(double... operandos);
}
