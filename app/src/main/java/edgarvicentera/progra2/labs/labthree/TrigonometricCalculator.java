package edgarvicentera.progra2.labs.labthree;


// https://www.w3schools.com/java/java_math.asp 

public class TrigonometricCalculator {

    public static double calculateSine(double degrees) {
        double radians = Math.toRadians(degrees);
        return Math.sin(radians);
    }

    public static double calculateCosine(double degrees) {
        double radians = Math.toRadians(degrees);
        return Math.cos(radians);
    }

    public static double calculateTangent(double degrees) {
        double radians = Math.toRadians(degrees);
        return Math.tan(radians);
    }

}

