package edgarvicentera.progra2.labs.labthree;


import java.util.List;

public interface GenericSecuence<T> {
    List<T> generarSecuencia(int n);
}
