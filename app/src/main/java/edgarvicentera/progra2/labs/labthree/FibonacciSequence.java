package edgarvicentera.progra2.labs.labthree;

import java.util.ArrayList;
import java.util.List;

public class FibonacciSequence {
  public List<Integer> generateSequence(int n) {
      List<Integer> sequence = new ArrayList<>();

      int a = 0;
      int b = 1;

      for (int i = 0; i < n; i++) {
          sequence.add(a);
          int temp = a;
          a = b;
          b = temp + b;
      }

      return sequence;
  }
}