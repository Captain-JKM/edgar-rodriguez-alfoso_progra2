# Ejercicio

Laboratorio 2 aplicando POO, extiende su funcionalidad de modo que puedas identificar y aplicar los conceptos de esta semana: Enums, Clases Abstractas, Interfaces. Por ejemplo, para agregar la implementación de estos conceptos en su solución previa plantee situaciones a su ejercicio concreto de modo que tenga sentido el uso de estos. 

# Mi defensa

 - En el #Main estaría la logica implementada de todas las clases por ejemplo un estudiante de Jala que quiera usar tal operación, dado que no pidio el profesor el uso de un main como tal intente solamente a modo de muestra y puede observar que en unos case puede recibir un assert para simular esa entrada pero igual use un sout solo para ser ilustrada y recibe los argumentos de la clase estudiante y la clase calculadora avanzada
 - Log, es donde se guardaría ese estudiante que se loguee con su x cuenta y queda registrada la calculadora personal y podemos simular ahi un registro tambien que puede ser llamado de las otras clases

 - Dado que no solo nos limitamos a operaciones sino en un uso como tal del estudiante

 - Tenemos algunas clases adicionales que nos apoyan pedidas en el laboratorio como Form que es el Enum que nos podría decir que forma geometrica ocuparemos

 ### Interfaces

 - #PrintObjectInterface es la interface que nos permite imprimir los datos del estudiante 

 - GenericSecuence nos permite ser un poco mas flexible a la hora de recibir los datos para esos numeros aleatorios que puede recibir el programa

 - #MathOperations se implementa como interface de las #BasicOperation calculator para hacer algunas operaciones, estas pueden implementarse sin ella aunque lo ideal era hacerlo con esto, intente plasmarlo ya que pueden requerir esos **operadores** para las operaciones 

### Abstract
- #GeometricAbstract es la clase abstracta que nos ayuda para ser una plantilla para ser usada por diversas clases en este caso formas geometricas como cirulos, cuadrados, triangulos, pero en esta ocasión solo será a la clase **CircleAreaCalculator** que lo implementa


### Enum
- La clase form hace la labo de Enum, esta es lo que se me ocurrio para usarla en los trigonometricos para saber si evaluamos alguna figura o puede tener un uso para un estudiante si puede ser **Profesor**, **Estudiante**, **Visitante**, podría tener esos usos 


# Casos de Uso

- Necesito como estudiante un Main para trabajar mis operaciones
- Necesito como administrador/estudiante un Log para ver mi historial 
- Necesito una clase que administre la logica de mis operaciones CalculadoraAvanzada



# Clases
 - BasicOperator
 - CalculadoraAvanzada
 - CircleAreaCalculator
 - Estudiante
 - FibonacciSequence
 - Form
 - GenericSecuence
 - GeometricAbstract
 - Log
 - Main
 - MathOperations
 - PrintObjectInterface
 - QuadraticEquationSolver
 - RandomNumberSequence
 - Register
 - TrigonometricCalculator


# Clases Abstractas

- GeometricAbstract
- QuadraticInterface

# Interfaces

- MathOperations
- PrintObjectInterface
- GenericSecuence


# Diagrams
### Calculadora Avanzada
![Alt text](Test/Diagrams/01-LS3-CA-D-2024-01-29-031317.png)

### Interface
![Alt text](Test/Diagrams/03-LS3-Inteface-D.png)

### Abstract
![Alt text](Test/Diagrams/02-LS3-ABSTRACT-D-2024-01-29-033003.png)

# Anotaciones

    Interfaz:
        Una interfaz en Java es un tipo de referencia que define un conjunto de métodos abstractos y, opcionalmente, métodos predeterminados y estáticos.
        Todas las declaraciones de método en una interfaz son implícitamente públicas y abstractas.
        Puedes implementar múltiples interfaces en una sola clase.
        Una clase puede implementar una interfaz sin tener que heredar de una clase base específica.
        Las interfaces se utilizan comúnmente para definir contratos que deben seguir las clases que las implementan, lo que permite la creación de clases polimórficas y la programación orientada a interfaces.

    Clase abstracta:
        Una clase abstracta es una clase que no puede ser instanciada directamente, sino que se usa como base para otras clases. Puede contener métodos abstractos (métodos sin implementación) y métodos concretos (métodos con implementación).
        Puede contener campos y métodos no abstractos, lo que permite proporcionar una implementación predeterminada.
        Puedes extender solo una clase abstracta en Java debido a la herencia única.
        Una clase abstracta puede tener constructores, mientras que una interfaz no puede tenerlos.