package edgarvicentera.progra2.labs.labthree;

import java.util.List;

public class CalculadoraAvanzada {

    private CircleAreaCalculator circleAreaCalculator;
    private QuadraticEquationSolver quadraticEquationSolution;
    private FibonacciSequence fibonacciSequence;
    private RandomNumberSequence randomNumberSequence;

    // Constructor que acepta un objeto Estudiante como parámetro
    public CalculadoraAvanzada(Estudiante estudiante) {
        this.circleAreaCalculator = new CircleAreaCalculator(0, Form.CIRCULO); 
        this.quadraticEquationSolution = new QuadraticEquationSolver();
        this.fibonacciSequence = new FibonacciSequence();
        this.randomNumberSequence = new RandomNumberSequence();
    }

    // Métodos específicos para cada operación
    public int sumar(int a, int b) {
        return (int) new BasicOperators.Suma().calculate(a, b); // Modificado para usar BasicOperators.Suma
    }
    
    // Algunos metodos piden ser llamados de forma directa.
    public int restar(int a, int b) {
        return BasicOperators.subtract(a, b); 
    }
    

    public int multiplicar(int a, int b) {
        return BasicOperators.multiply(a, b);
    }

    public int dividir(int a, int b) {
        return BasicOperators.divide(a, b);
    }

    public double calcularAreaCirculo(double radio) {
        return circleAreaCalculator.calcularArea(); // Modificado para calcular el área directamente
    }

    public double calcularPerimetroCirculo(double radio) {
        return circleAreaCalculator.calcularPerimetro(); // Nuevo método para calcular el perímetro
    }

    public double calcularSeno(double degrees) {
        return TrigonometricCalculator.calculateSine(degrees);
    }

    public double calcularCoseno(double degrees) {
        return TrigonometricCalculator.calculateCosine(degrees);
    }

    public double calcularTangente(double degrees) {
        return TrigonometricCalculator.calculateTangent(degrees);
    }

    public double[] resolverEcuacionCuadratica(double a, double b, double c) {
        return quadraticEquationSolution.solveQuadraticEquation(a, b, c);
    }

    public List<Integer> generarSecuenciaFibonacci(int n) {
        return fibonacciSequence.generateSequence(n);
    }

    public List<Integer> generarSecuenciaNumerosAleatorios(int length) {
        return randomNumberSequence.generarSecuencia(length); 
    }
}
