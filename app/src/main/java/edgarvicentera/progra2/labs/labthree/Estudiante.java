package edgarvicentera.progra2.labs.labthree;

public class Estudiante implements PrintObjectInterface {
    private String nombre;
    private String apellido;
    private String matricula;

    public Estudiante(String nombre, String apellido, String matricula) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.matricula = matricula;
    }

    // Getters y setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Override
    public void imprimir() {
        System.out.println("Nombre: " + nombre + ", Apellido: " + apellido + ", Matricula: " + matricula);
    }
}
