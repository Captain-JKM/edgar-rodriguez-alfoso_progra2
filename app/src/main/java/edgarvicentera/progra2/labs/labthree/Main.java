package edgarvicentera.progra2.labs.labthree;

import java.util.Scanner;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

      
        System.out.println("Bienvenido, por favor ingrese su nombre:");
        String nombre = scanner.nextLine();
        System.out.println("Bienvenido, por favor ingrese su Apellido:");
        String apellido = scanner.nextLine();
        System.out.println("Bienvenido, por favor ingrese su matrícula:");
        String matricula = scanner.nextLine(); 

        //Nuevo estudiante
        Estudiante student = new Estudiante(nombre, apellido, matricula); 


        // Enlazamos al estudiante a su calculadora
        CalculadoraAvanzada calculadora = new CalculadoraAvanzada(student);

      
        System.out.println("Hola, " + nombre + "! ¿Qué operación desea realizar?");
        System.out.println("1. Sumar");
        System.out.println("2. Restar");
        System.out.println("3. Multiplicar");
        System.out.println("4. Dividir");
        System.out.println("5. Calcular área de un círculo");
        System.out.println("6. Generar secuencia de Fibonacci");
        System.out.println("7. Generar secuencia de números aleatorios");

        // Leer la opción de operación seleccionada por el usuario
        int option = scanner.nextInt();

        // Sentencia Switch
        switch (option) {
            case 1:
                // Sumar con el formato de un assert para hacer los test
                double resultadoSuma = calculadora.sumar(10, 5);
                assert resultadoSuma == 15 : "Error en la suma";

                break;
            case 2:
                // Restar con la forma normal de un sout
                /*
                System.out.println("Ingrese el primer número:");
                int num3 = scanner.nextInt();
                System.out.println("Ingrese el segundo número:");
                int num4 = scanner.nextInt();
                int resultadoResta = calculadora.restar(num3, num4);
                System.out.println("El resultado de la resta es: " + resultadoResta);

                 */
                break;
            case 3:
                // Multiplicar
                // Mostrar en vivo en la defensa
                break;
            case 4:
                // Dividir
                
                break;
            case 5:
                // Calcular área de un círculo
                System.out.println("Ingrese el radio del círculo:");
                double radio = scanner.nextDouble();
                double resultadoAreaCirculo = calculadora.calcularAreaCirculo(radio);
                System.out.println("El área del círculo es: " + resultadoAreaCirculo);
                break;
            case 6:
                // Generar secuencia de Fibonacci
                System.out.println("Ingrese la cantidad de números en la secuencia de Fibonacci:");
                int n = scanner.nextInt();
                List<Integer> secuenciaFibonacci = calculadora.generarSecuenciaFibonacci(n);
                System.out.println("La secuencia de Fibonacci es: " + secuenciaFibonacci);
                break;
            case 7:
                // Generar secuencia de números aleatorios
                System.out.println("Ingrese la longitud de la secuencia:");
                int length = scanner.nextInt();
                List<Integer> secuenciaAleatoria = calculadora.generarSecuenciaNumerosAleatorios(length);
                System.out.println("La secuencia de números aleatorios es: " + secuenciaAleatoria);
                break;
            default:
                System.out.println("Opción no válida");
        }

        scanner.close();
    }
}
