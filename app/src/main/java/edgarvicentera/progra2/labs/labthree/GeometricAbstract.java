package edgarvicentera.progra2.labs.labthree;

public abstract class GeometricAbstract {
    public abstract double calcularArea();
    public abstract double calcularPerimetro();
}
