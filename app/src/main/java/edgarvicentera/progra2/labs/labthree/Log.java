package edgarvicentera.progra2.labs.labthree;
//import para la defensa
//import java.util.List;

public class Log {
    public void run() {

        //Estudiante de Jala que usa la calculadora
        Estudiante student1 = new Estudiante("Diana", "Romero", "20230001");
        Estudiante student2 = new Estudiante("María", "González", "20230002");

        //Calculadora del Estudiante
        CalculadoraAvanzada calculadora1 = new CalculadoraAvanzada(student1);
        CalculadoraAvanzada calculadora2 = new CalculadoraAvanzada(student2);


        // Ejemplo de uso de la calculadora con un estudiante específico
        double resultadoSuma = calculadora1.sumar(10, 5);
        assert resultadoSuma == 15 : "Error en la suma";

        double resultadoResta = calculadora2.restar(34, 2);
        assert resultadoResta == 32 : "Error en la restado";

    }
}