package edgarvicentera.progra2.labs.labthree;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomNumberSequence implements GenericSecuence<Integer> {

    private final Random random;

    public RandomNumberSequence() {
        this.random = new Random();
    }

    public int getNextRandomInt() {
        return random.nextInt();
    }

    @Override
    public List<Integer> generarSecuencia(int length) {
        if (length <= 0) {
            throw new IllegalArgumentException("La longitud debe ser mayor que cero");
        }

        List<Integer> randomSequence = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            randomSequence.add(random.nextInt());
        }
        return randomSequence;
    }
}
