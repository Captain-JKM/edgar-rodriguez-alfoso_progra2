package edgarvicentera.progra2.labs.labthree;




public class QuadraticEquationSolver extends QuadraticInterface {
    
    @Override
    public double operar(double a, double b) {
        return 0; 
    }

    public double[] solveQuadraticEquation(double a, double b, double c) {

        // Calculo del discriminante de la forma b^2 - 4ac 

        double discriminant = b * b - 4 * a * c;

        if (discriminant > 0) {

            // Dos soluciones reales distintas
            double root1 = (-b + Math.sqrt(discriminant)) / (2 * a);
            double root2 = (-b - Math.sqrt(discriminant)) / (2 * a);
            return new double[]{root1, root2};
        } else if (discriminant == 0) {

            // Una solución real (raíz doble)
            double root = -b / (2 * a);
            return new double[]{root};
        } else {
            
            // Sin soluciones reales
            return new double[]{};
        }
    }
}