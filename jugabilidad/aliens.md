---
description: Aquí explicamos nuestros alienas
cover: ../.gitbook/assets/speedpainting___portals___by_antifan_real_d5nzw9s.jpg
coverY: 0
---

# Aliens

## History

En un rincón olvidado del universo, los Xenolites, una raza alienígena al borde de la extinción, buscan desesperadamente un nuevo hogar. Tras la destrucción de su mundo natal, dirigen su mirada hacia la Tierra, un planeta rico en recursos. La humanidad, alertada de la invasión, se prepara para resistir. En el cielo, las naves Xenolites se acercan, desatando una batalla épica por la supervivencia de ambos mundos. La guerra ha comenzado, y el destino de la Tierra está en juego.



## Aliens

[Segundo paquete de naves](https://foozlecc.itch.io/void-fleet-pack-3)

Los buscarán activamente destruir a nuestro jugador con el fin de lograr su objetivo y cumplir con conquistar la tierra.

#### Special Alien

<figure><img src="../.gitbook/assets/j+iMdX.gif" alt=""><figcaption></figcaption></figure>

#### Attack Ship

<figure><img src="../.gitbook/assets/I2_Eeo.gif" alt=""><figcaption></figcaption></figure>



#### Offensive ship

<figure><img src="../.gitbook/assets/aTsOuF.gif" alt=""><figcaption></figcaption></figure>



#### Protection Ship

<figure><img src="../.gitbook/assets/fjnPOu (1).gif" alt=""><figcaption></figcaption></figure>

## Movimiento

Con el movimiento batalle muchísimo debido a que quería hacer uso de un storyboard para ahorrarme la lígica esta la podía llamar y hacer uso de un timer del cual pudiera implementarlo pero no fue posible debido a que enfrentaba algo más complicado el hitbox este se descuadraba de las posiciones porque aunque aparecieran los hitbox no lo hacían.

```csharp
        public static void StartCanvasMovementAnimation(Canvas aliensCanvas, double from, double to, double durationInSeconds, bool autoReverse = true)
        {
            var storyboard = new Storyboard();

            var translateAnimation = new DoubleAnimation
            {
                From = from,
                To = to,
                Duration = new Duration(TimeSpan.FromSeconds(durationInSeconds)),
                AutoReverse = autoReverse,
                RepeatBehavior = RepeatBehavior.Forever
            };

            if (aliensCanvas.RenderTransform == null || !(aliensCanvas.RenderTransform is TranslateTransform))
            {
                aliensCanvas.RenderTransform = new TranslateTransform();
            }

            Storyboard.SetTarget(translateAnimation, aliensCanvas.RenderTransform);
            Storyboard.SetTargetProperty(translateAnimation, "X");

            storyboard.Children.Add(translateAnimation);
            storyboard.Begin();
        }
```

La mejor alternativa fue la que pude encontrar con un par de sugerencias debido a que en mi caso no supe como traer las posiciones de la pantalla por más que quise y cuando quería centrar una entonces se perdía la otra.&#x20;

```csharp
    public void MoveAliens()
    {
        bool aliensReachBound = false;
        foreach (var alien in _alienShips)
        {
            if (alien.BaseShipPositionX + _alienMoveStep > _alienMoveBoundary || alien.BaseShipPositionX + _alienMoveStep < 0)
            {
                aliensReachBound = true;
                break;
            }
        }

        if (aliensReachBound)
        {
            _alienMoveDirection = -_alienMoveDirection;
            foreach (var alien in _alienShips)
            {
                alien.BaseShipPositionY += 7;
            }
        }

        foreach (var alien in _alienShips)
        {
            alien.BaseShipPositionX += _alienMoveDirection;
        }
    }
```

Cada que se hace una llamada de `MoveAliens` que siempre es inicializada en false y entonces empieza a invocar un par de llamadas si alguna de las naves alcanza el límite izquierdo o derecho entonces se establece el booleano en true, entonces primero se invierte la dirección y se cambia la posición gracias a estas dos variables y se desciende hacía abajo y lo que se ejecuta el inicio del movimiento es e foreach que va actualizando el movimiento de nuestro alien en la posición en X.

Si una vez que se llama de nuevo se reinicia el ciclo y se setea nuevamente en false.

## Rango de ataque

Primero se encarga de filtrar las naves que pertenecen al tipo AlienTierOne que son las naves de ataque o puede ser cualquiera que sea de la tier de 1 a 3 y ya el random tiene una probabiblidad del 0 y 1 dado que el 0.02 representa de que un disparo ocurra con un 2% de probabilidad.

```csharp
   var attackShips = AlienShips.Where(alien => alien.AlienTier == AlienTierEnum.One).ToList();
   if (attackShips.Any() && new Random().NextDouble() < 0.02) 
   {
       var randomAttackShip = attackShips[new Random().Next(attackShips.Count)];
       AttackShipShoot(randomAttackShip);
   }
```

## Spawn Aliens

Aquí tuve un reto bastante interesante ya que para mi fue uno de los desafíos más complicados que he tenido a lo largo de este proyecto debido a que no podía centrarlos en la pantalla, no conseguía que estos se posicionará de manera eficiente y cuando lo conseguí no podía asignarles una puntuación.

```csharp
    private void AlienSpawnGrid()
    {
        double initialX = 50;
        double initialY = 50;
        double gapX = 100;
        double gapY = 60;

        string tierOneImageUri = "ms-appx:///Assets/Images/enemy-tier-one.png";

        foreach (var tier in new[] { tierOneImageUri, tierOneImageUri })
        {
            for (int i = 0; i < 8; i++)
            {
                Aliens.Add(new AlienShip(initialX + i * gapX, initialY, tier));
            }
            initialY += gapY;
        }
    }
```



La nueva implementación tiene muchas ventajas positivas ya que puedo crear una ObservableCollection de nombre AlienShip que usa la clase AlienShipModels

```csharp
 public void SpawnAlienGrid(int level)
 {
     double startAlienPositionX = 0;
     double startAlienPositionY = 50;
     double gapX = 100;
     double gapY = 60;

     int alienShipColumns = 8;

     _alienShips.Clear();

     for (int i = 0; i < alienShipColumns; i++)
     {
         var attackShip = new AlienShipModel(startAlienPositionX + i * gapX, startAlienPositionY, _attackShipEnemy, AlienTierEnum.One);
         _alienShips.Add(attackShip);

         for (int j = 0; j < 2; j++)
         {
             var defenseShip = new AlienShipModel(startAlienPositionX + i * gapX, startAlienPositionY + (gapY * (j + 1)), _defenseShipEnemy, AlienTierEnum.Two);
             _alienShips.Add(defenseShip);
         }

         for (int j = 0; j < 2; j++)
         {
             var protectionShip = new AlienShipModel(startAlienPositionX + i * gapX, startAlienPositionY + (gapY * (j + 3)), _protectionShipEnemy, AlienTierEnum.Three);
             _alienShips.Add(protectionShip);
         }
     }

     _alienMoveStep = 10 + (level * 2);
 }
```

## Formulas del calculo&#x20;

startAlienPositionX es la posición inicial del eje x de la primera columna.

startAlienPositionY es la posición inicial del eje Y para la primera columna.

gapx es la ditancia del eje X

gapY es la distancia entre las naves en el eje Y



| Col 1   | Col 2     |
| ------- | --------- |
| (0,50)  | (100,50)  |
| (0,110) | (100,110) |
| (0.170) | (100,170) |
| (0,230) | (100,230) |
| (0.290) | (100.290) |

