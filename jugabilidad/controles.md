---
cover: ../.gitbook/assets/image_2024-08-21_104804517.png
coverY: 0
---

# Controles

## Controles&#x20;

Es demasiado fácil moverte con nuestro jugador, puedes moverte usando la `arrow key left` o la `arrow key right` para desplazarte de izquierda a derecha en nuestra pantalla, no hemos habilitado el movimiento .

<figure><img src="../.gitbook/assets/image (25).png" alt=""><figcaption></figcaption></figure>

## Ataque

El ataque lo puedes hacer por medio de la barra espaciadora de tu teclado. El ataque se limitará a solamente una bala hasta que esta rebase el borde o esta impacque con un alien.

<figure><img src="../.gitbook/assets/image (26).png" alt=""><figcaption></figcaption></figure>

