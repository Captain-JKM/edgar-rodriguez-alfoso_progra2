---
description: Aqui explicaremos cosas esenciales de nuestro jugador
cover: ../.gitbook/assets/anime-style-character-space.jpg
coverY: 0
---

# Jugador

Nuestro jugador de Space Invaders nos ayudará a enfrentarnos contra los aliens que buscan activamente destruirnos por medio de una serie de oleadas que están en constante amenaza a nuestro sistema. Por en debemos de tener un jugador con fluidez de movimientos y que pueda atacar al alien.



## Diseños

<figure><img src="../.gitbook/assets/PlayerBase.png" alt=""><figcaption></figcaption></figure>

## Sprites

<figure><img src="../.gitbook/assets/player-weapon.png" alt=""><figcaption></figcaption></figure>

## Bala

<figure><img src="../.gitbook/assets/player-projectil.png" alt=""><figcaption></figcaption></figure>



## Clases

Tenemos dos clases que se encargan de modelar nuestro jugador, SpaceShip y Ship que es la clase de la que hereda nuestro SpaceShip.

## SpaceShip class



Es la que maneja la lógica de nuestro jugador que tiene implementada una interfaz de un Arma para que esta este ligado a nuestro usuario y podamos disparar desde la posición de nuestro usuario.

```csharp
using SpaceEvolution.Models.Interfaces;
namespace SpaceEvolution.Models;
public class PlayerShipModel : ShipBaseModel
{
    public IWeapon Weapon { get; set; }

    public int WidthShip = 50;
    public int HeightShip = 50;

    public PlayerShipModel(double x, double y) : base(x, y)
    {
    }

    public void Shoot()
    {
        Weapon?.Shoot(BaseShipPositionX, BaseShipPositionY);
    }

    internal Rectangle GetBoundingBox()
    {
        return new Rectangle((int)BaseShipPositionX, (int)BaseShipPositionY, WidthShip, HeightShip);
    }
}


```

### Interfaz

Implementamos una interfaz de coalisión y creamos un método GetBounding del cual este siempre recibe 4 parámetros cada que creamos un rectángulo. Entonces ligamos la posición de X,Y, Widh heigh siempre en enteros este no maneja flotantes pero no es problema sigue siendo eficiente.

```csharp
using System.Drawing;

namespace SpaceEvolution.Models.Interface;
public interface ICoalisionBox
{
    Rectangle GetBoundingObjectBox();
}

```

[https://learn.microsoft.com/es-es/dotnet/api/system.drawing.rectangle?view=net-8.0](jugador.md#interfaz)

### Ship Class

Es donde heredan nuestras naves para detectar su posición donde se declara esta clase la heredan las naves aliadas y nuestro jugador.

```csharp
namespace SpaceEvolution.Models; public abstract class Ship : ObservableObject { private double _positionX; private double _positionY;
public double PositionX
{
    get => _positionX;
    set
    {
        if (_positionX != value)
        {
            _positionX = value;
            OnPropertyChanged();
        }
    }
}

public double PositionY
{
    get => _positionY;
    set
    {
        if (_positionY != value)
        {
            _positionY = value;
            OnPropertyChanged();
        }
    }
}

protected Ship(double x, double y)
{
    PositionX = x;
    PositionY = y;
}

public void Move(double movX, double movY)
{
    PositionX += movX;
    PositionY += movY;
    OnPropertyChanged(nameof(PositionX));
    OnPropertyChanged(nameof(PositionY));
}
}
```
