# Table of contents

* [Space Evolution - Overview](README.md)

## Lo básico:

* [Entorno de desarrollo](lo-basico/entorno-de-desarrollo.md)
* [UI](lo-basico/ui.md)
* [Arquitectura MVVM](lo-basico/arquitectura-mvvm/README.md)
  * [Estructura de Carpetas](lo-basico/arquitectura-mvvm/estructura-de-carpetas.md)

## Jugabilidad

* [Jugador](jugabilidad/jugador.md)
* [Aliens](jugabilidad/aliens.md)
* [Controles](jugabilidad/controles.md)

## ESENCIAL

* [GameOver](esencial/gameover.md)
* [Manejo de archivos](esencial/manejo-de-archivos.md)
* [Score](esencial/score.md)
